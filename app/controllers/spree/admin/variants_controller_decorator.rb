module Spree
  module Admin
    VariantsController.class_eval do
      include SubmittableBehaviors
      create.before :attach_create_resource_type

      alias_method :old_edit, :edit
      def edit
        populate_form_values
        old_edit
      end

      private
      alias_method :old_new_before, :new_before
      def new_before
        old_new_before
        populate_form_values
      end

      def populate_form_values
        Spree::VariantPresenter.new(@object.product.master).form_values.each do |form_value|
          unless @object.form_values.any?(&value_check(form_value))
            @object.form_values.build(custom_field: form_value.custom_field, value: form_value.value)
          end if @object.product.resource_type.present?
        end
      end

      def attach_create_resource_type
        @object.resource_type = @object.product.resource_type
      end

      def value_check(mv)
        Proc.new do |vv|
          mv.custom_field == vv.custom_field && mv.value == vv.value
        end
      end
    end
  end
end
