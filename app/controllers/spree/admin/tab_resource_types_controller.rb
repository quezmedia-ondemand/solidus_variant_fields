module Spree
  module Admin
    class TabResourceTypesController < Spree::Admin::BaseController
      include SubmittableBehaviors
      before_action :find_product
      helper_method :formatted_value

      def index
        @resource_type = @product.resource_type
      end

      # It actually updates
      def update
        @variant = Spree::Variant.find(params[:id])
        @resource_type = @product.resource_type

        @variant.assign_attributes(variant_submittable_params)
        @variant.form_values.map(&:save) # It tries to save each one on itself.
        if @variant.save
          respond_with(@product) do |format|
            format.html { redirect_to(
              spree.admin_product_tab_resource_types_path(product_id: @product.id),
              flash: { notice: Spree.t(:values_updated, scope: [:admin, :resource_types]) })
            }
          end
        else
          render :index
        end
      end

      def delete_value
        form_value = Spree::FormValue.find(params[:form_value_id])
        if form_value.destroy
          respond_with(@product) do |format|
            format.html { redirect_to(
              spree.admin_product_tab_resource_types_path(product_id: @product.id),
              flash: { notice: Spree.t(:value_deleted, scope: [:admin, :resource_types]) })
            }
          end
        else
          render :index
        end
      end

      def formatted_value(value)
        if value.is_a?(Array)
          value.reject(&:blank?).join(',')
        else
          value
        end
      end

      private
      def find_product
        @product = Spree::Product.friendly.find(params[:product_id])
      end

      def variant_submittable_params
        params.require(:variant).permit(form_values_attributes)
      end
    end
  end
end
