module Spree
  module Admin
    module ResourceTypes
      class CustomFieldsController < ResourceController
        append_before_action :load_resource, :filter_by_resource_type, except: :update_positions

        def update_values_positions
          params[:positions].each do |id, index|
            @custom_fields.find(id).update_column(:position, index)
          end

          respond_to do |format|
            format.js { render plain: 'Ok' }
          end
        end

        private
        def filter_by_resource_type
          @custom_fields = collection.where(
            owner_id: params[:resource_type_id],
            owner_type: 'Spree::ResourceType')
        end
      end
    end
  end
end
