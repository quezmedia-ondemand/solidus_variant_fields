module Spree
  module Admin
    class ResourceTypesController < ResourceController
      include FormBehaviors

      def update_values_positions
        params[:positions].each do |id, index|
          Spree::ResourceType.where(id: id).update_all(position: index)
        end

        respond_to do |format|
          format.html { redirect_to admin_product_variants_url(params[:product_id]) }
          format.js { render plain: 'Ok' }
        end
      end

      private

      def location_after_save
        edit_admin_resource_type_url(@resource_type)
      end

      def load_product
        @product = Spree::Product.find_by_param!(params[:product_id])
      end

      def permitted_resource_params
        params.require(:resource_type).permit(:name, custom_fields_attributes)
      end
    end
  end
end
