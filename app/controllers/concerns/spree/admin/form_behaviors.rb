module Spree
  module Admin
    module FormBehaviors
      extend ActiveSupport::Concern

      def custom_fields_attributes
        [custom_fields_attributes:
          [:id, :_destroy, :label, :placeholder, :required, :type,
            :min, :max, :as_check_boxes, :frontend_visible,
            options_attributes: [:id, :_destroy, :label]]]
      end
    end
  end
end
