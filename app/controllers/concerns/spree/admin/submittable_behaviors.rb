module Spree
  module Admin
    module SubmittableBehaviors
      extend ActiveSupport::Concern

      def form_values_attributes
        [form_values_attributes:
          [:id, :_destroy, :custom_field_id, :value, value: []]
        ]
      end
    end
  end
end
