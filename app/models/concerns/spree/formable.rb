module Spree
  module Formable
    extend ActiveSupport::Concern

    included do
      has_many :custom_fields,                  as: :owner,
                                                class_name: Spree::CustomField,
                                                inverse_of: :owner,
                                                dependent: :destroy
      accepts_nested_attributes_for :custom_fields, allow_destroy: true

      scope :with_fields, ->(){ includes(:custom_fields)
        .where.not(spree_custom_fields: {owner_id: nil})
        .where(spree_custom_fields: {owner_type: self.name}) }
    end
  end
end
