module Spree
  module Submittable
    extend ActiveSupport::Concern

    included do
      has_many :form_values,                  class_name: Spree::FormValue,
                                              as: :holder
      has_many :frontend_form_values,         ->(){ only_frontend_visible },
                                              as: :holder,
                                              class_name: Spree::FormValue

      accepts_nested_attributes_for :form_values, allow_destroy: true, reject_if: :all_blank
    end
  end
end
