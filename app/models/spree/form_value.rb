module Spree
  class FormValue < Spree::Base
    after_save :touch_holder
    serialize :value
    # before_validation :format_value, if: Proc.new{|f| f.custom_field.class == Spree::DropdownField }
    belongs_to :custom_field,               class_name: Spree::CustomField,
                                            inverse_of: :form_value,
                                            foreign_key: 'custom_field_id'


    # This is going to be the submittable. The Model Who Holds the answer...
    belongs_to :holder, polymorphic: true

    validate :value_is_valid, if: Proc.new{|f| f.custom_field.present? }

    default_scope { order(:created_at) }

    # Scopes
    def self.only_frontend_visible
      frontend_visible_form_values_ids = where(frontend_visible: true)
      frontend_visible_custom_fields_ids = joins(:custom_field).where(spree_custom_fields: { frontend_visible: true })
      self.where(id: (frontend_visible_form_values_ids + frontend_visible_custom_fields_ids).uniq)
    end

    def field_label
      custom_field.present?? custom_field.label : super
    end

    private

    def value_is_valid
      custom_field.validate_value(self)
    end

    def touch_holder
      holder.update_column(:updated_at, Time.current)
    end
  end
end
