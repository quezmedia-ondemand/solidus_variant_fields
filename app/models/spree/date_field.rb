module Spree
  class DateField < Spree::CustomField
    validates :options,
              absence: { message: "can't be specified on date field" }

    validate :date_limits

    def add_validation_errors(form_value)
      value = form_value.value
      return if form_value.blank?

      unless valid_date?(value)
        form_value.errors.add :value, "#{label} must be a valid date of format yyyy/mm/dd"
        return
      end

      date = Date.parse(value)

      if min.present?
        min_date = Date.parse(min)
        form_value.errors.add :value, "#{label} can't be before #{min_date}" if date < min_date
      end
      if max.present?
        max_date = Date.parse(max)
        form_value.errors.add :value, "#{label} can't be after #{max_date}" if date > max_date
      end
    end

    private

    def valid_date?(date_string)
      begin
        Date.strptime(date_string, "%Y/%m/%d")
      rescue ArgumentError
         return false
      end
      return true
    end

    def date_limits
      invalid_date_message = "is not a valid date. It should be of format yyyy/mm/dd"
      self.errors.add :min, invalid_date_message if min.present? && !valid_date?(min)
      self.errors.add :max, invalid_date_message if max.present? && !valid_date?(max)
    end
  end
end
