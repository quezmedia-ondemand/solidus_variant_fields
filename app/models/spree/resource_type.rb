module Spree
  class ResourceType < Spree::Base
    include Formable
    acts_as_list
    after_save :touch_variants

    has_many :variants,               class_name: Spree::Variant,
                                      foreign_key: 'resource_type_id',
                                      inverse_of: :resource_type,
                                      dependent: :nullify

    validates :name, presence: true, uniqueness: true

    default_scope { order(position: :asc) }

    private
    def touch_variants
      variants.update_all(updated_at: Time.current)
    end
  end
end
