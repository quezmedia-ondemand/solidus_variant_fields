module Spree
  class CustomField < Spree::Base
    acts_as_list
    before_save :fulfill_placeholder, if: lambda { |c| c.placeholder.empty? }
    after_save :destroy_options, if: lambda { |c| c.type != "Spree::DropdownField" && c.options.any? }
    after_save :touch_owner
    after_destroy :adjust_form_value
    belongs_to :owner, polymorphic: true
    has_one :form_value,                  class_name: Spree::FormValue,
                                          inverse_of: :custom_field,
                                          foreign_key: 'custom_field_id',
                                          dependent: :nullify

    has_one :taxon_filter,                class_name: Spree::Filter,
                                          inverse_of: :custom_field,
                                          foreign_key: 'custom_field_id',
                                          dependent: :nullify
    delegate :value, to: :form_value

    validates :label, :type, presence: true

    has_many :options,        class_name: Spree::DropdownOption,
                              foreign_key: 'dropdown_field_id',
                              inverse_of: :dropdown_field,
                              dependent: :destroy

    accepts_nested_attributes_for :options, allow_destroy: true

    default_scope { order(position: :asc) }

    def validate_value(form_value)
      value = form_value.value
      value = value.delete_if(&:blank?) if value.is_a?(Array)
      if required? && value.blank?
        form_value.errors.add :value, "#{label} can't be blank"
        add_validation_errors(form_value)
      end

      form_value
    end

    def add_validation_errors(form_value)
      fail NotImplementedError
    end

    private
    def destroy_options
      self.options.destroy_all
    end

    def fulfill_placeholder
      placeholder = label
    end

    def adjust_form_value
      if form_value
        form_value.update_columns({field_label: label, frontend_visible: frontend_visible})
      end
    end

    def touch_owner
      owner.update_attribute(:updated_at, Time.current)
    end
  end
end
