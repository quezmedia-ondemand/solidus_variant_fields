module Spree
  Product.class_eval do
    [:resource_type, :resource_type_id].each do |attr|
      delegate :"#{attr}", :"#{attr}=", to: :find_or_build_master
    end

    accepts_nested_attributes_for :master, allow_destroy: true, reject_if: :all_blank

    def instance_variant_with_form_values
      if master.frontend_form_values.any?
        instance_variant = master
      elsif variants.first.present? && variants.first.frontend_form_values.any?
        instance_variant = variants.first
      end
      instance_variant
    end
  end
end
