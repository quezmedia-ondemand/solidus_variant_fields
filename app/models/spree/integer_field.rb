module Spree
  class IntegerField < Spree::CustomField
    validate :number_limits

    def add_validation_errors(form_value)
      value = form_value.value
      unless value.blank? || value.match(/\A[+-]?\d+\Z/)
        form_value.errors.add :value, "#{label} must be an integer"
      end

      int_value = value.to_i
      if max.present? && int_value > max.to_i
        form_value.errors.add :value, "#{label} can't be greater than #{max}."
      end

      return unless min.present? && int_value < min.to_i

      form_value.errors.add :value, "#{label} can't be less than #{min}."
    end

    private
    def valid_number?(number_string)
      begin
        Integer(number_string)
      rescue ArgumentError
         return false
      end
      return true
    end

    def number_limits
      invalid_number_message = "is not a number."
      self.errors.add :min, invalid_number_message if min.present? && !valid_number?(min)
      self.errors.add :max, invalid_number_message if max.present? && !valid_number?(max)
    end
  end
end
