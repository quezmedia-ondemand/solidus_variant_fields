module Spree
  Variant.class_eval do
    include Submittable

    belongs_to :resource_type,              class_name: Spree::ResourceType,
                                            foreign_key: 'resource_type_id',
                                            inverse_of: :variants

    delegate :custom_fields, to: :resource_type

    def hash_values
      frontend_form_values.reject{ |f| f.value.empty? }.
        sort_by{|f| f.custom_field.present?? f.custom_field.created_at : f.created_at }.map do |form_value|
        [form_value.field_label.gsub(/\s/,"_"), form_value.value]
      end.to_h.to_json
    end
  end
end
