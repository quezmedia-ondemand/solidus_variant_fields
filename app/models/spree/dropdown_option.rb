module Spree
  class DropdownOption < Spree::Base
    belongs_to :dropdown_field,         class_name: Spree::DropdownField,
                                        foreign_key: 'dropdown_field_id',
                                        inverse_of: :options

    validates :label, presence: true

    default_scope { order(position: :asc) }
  end
end
