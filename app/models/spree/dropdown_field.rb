module Spree
  class DropdownField < Spree::CustomField
    validates :options, presence: true
    validate :at_least_one_option

    def add_validation_errors(form_value)
      labels = options.pluck(:label)
      if as_check_boxes
        unless labels.any?{|l| form_value.value.include?(l)}
          form_value.errors.add :value, "#{label} should be at least one of the following options: #{labels.join(", ")}"
        end
      else
        unless labels.any?{ |l| l == form_value.value }
          form_value.errors.add :value, "#{label} should one of the following options: #{labels.join(", ")}"
        end
      end
    end

    private
    def at_least_one_option
      if options.size < 2
        errors.add(:options, "#{label} should have at least one option")
      end
    end
  end
end
