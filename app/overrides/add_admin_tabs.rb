Deface::Override.new(:virtual_path => "spree/admin/shared/_product_tabs",
                     :name => "resource_type_admin_product_tabs",
                     :insert_bottom => "[data-hook='admin_product_tabs']",
                     :partial => "spree/admin/shared/resource_type_product_tabs",
                     :original => '31f61d84eceec2eb7b2f850a697fe660385adbb8',
                     :disabled => false)
