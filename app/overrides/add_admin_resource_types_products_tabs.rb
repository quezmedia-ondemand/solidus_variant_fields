Deface::Override.new(:virtual_path => "spree/admin/shared/_product_sub_menu",
                     :name => "product_resource_types",
                     :insert_bottom => "[data-hook='admin_product_sub_tabs']",
                     :partial => "spree/admin/shared/product_resource_types_tab",
                     :original => 'd23f2b0c56b85a606bf839969cc9a893af7f3043',
                     :disabled => false)
