Deface::Override.new(:virtual_path => "spree/products/show",
                    :name => "product_resource_type_values",
                    :insert_after => "div[data-hook='product_description'] div[data-hook='description']",
                    :partial => "spree/products/resource_type_values",
                    :original => '6cd4989870a4963435398a82cb2a04af3d60e3c8',
                    :disabled => false)

Deface::Override.new(:virtual_path => "spree/products/_cart_form",
                    :name => "radio_variant_resource_fields_sibling",
                    :insert_after => "erb[loud]:contains('radio_button_tag')",
                    :partial => "spree/products/resource_fields_element_radio_sibling",
                    :original => 'e0311e2467b5e79a5cd7236c47dd93ad39753021',
                    :disabled => false)
