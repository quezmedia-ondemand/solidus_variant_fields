Deface::Override.new(:virtual_path => "spree/admin/variants/_form",
                     :name => "variant_resource_type",
                     :insert_after => "div[data-hook='variants'] .row .col-xs-3:last",
                     :partial => "spree/admin/variants/resource_type_field",
                     :original => 'e23a6ea624f29cac492fc60454fedf59dbf1c2d3',
                     :disabled => false)

Deface::Override.new(:virtual_path => "spree/admin/products/new",
                    :name => "master_variant_resource_type",
                    :insert_after => "div[data-hook='new_product_tax_category']",
                    :partial => "spree/admin/products/resource_type_field",
                    :original => '99aa040cc2c4279afc88d129238c859793302b52',
                    :disabled => false)

Deface::Override.new(:virtual_path => "spree/admin/products/_form",
                    :name => "edit_master_variant_resource_type",
                    :insert_after => "div[data-hook='admin_product_form_tax_category']",
                    :partial => "spree/admin/products/edit_resource_type_field",
                    :original => 'fe0eb9d85bee5c633944a11cab86c941df2a8ca3',
                    :disabled => false)


Deface::Override.new(:virtual_path => "spree/admin/variants/_form",
                    :name => "variant_resource_types",
                    :insert_after => "div[data-hook='admin_variant_form_fields']",
                    :partial => "spree/admin/variants/resource_types",
                    :original => 'c1ed6af80f933f53cd8a0780ed2bb49b77fb570d',
                    :disabled => false)
