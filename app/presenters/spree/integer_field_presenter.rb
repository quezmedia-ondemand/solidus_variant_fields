module Spree
  class IntegerFieldPresenter < Spree::CustomFieldPresenter
    def options
      super.merge({as: :integer})
    end
  end
end
