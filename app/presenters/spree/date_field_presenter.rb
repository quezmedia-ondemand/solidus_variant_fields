module Spree
  class DateFieldPresenter < Spree::CustomFieldPresenter
    def options
      super.merge({as: :string,
        min: Date.strptime(min, "%Y/%m/%d"),
        max: Date.strptime(max, "%Y/%m/%d")})
    end

    def input_html
      super.merge({class: 'datepicker'}, &merge_adjustments)
    end
  end
end
