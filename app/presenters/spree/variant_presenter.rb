module Spree
  class VariantPresenter < SimpleDelegator
    def self.decorate_collection(variants)
      variants.map { |v| new(v) }
    end

    def object
      __getobj__
    end

    def form_values
      object.custom_fields.each do |f|
        unless object.form_values.pluck(:custom_field_id).any?{|c_id| c_id == f.id}
          object.form_values.build(custom_field: f)
        end
      end if object.resource_type.present?
      object.form_values.sort_by{|f| f.custom_field.present?? f.custom_field.created_at : f.created_at }
    end

  end
end
