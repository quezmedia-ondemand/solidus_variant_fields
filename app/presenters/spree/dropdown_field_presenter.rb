module Spree
  class DropdownFieldPresenter < Spree::CustomFieldPresenter
    def options
      super.merge({collection: object.options,
        label_method: :label, value_method: :label}).merge(inner_options)
    end

    def input_html
      super.merge({class: 'select2'}, &merge_adjustments)
    end

    private
    def inner_options
      if as_check_boxes?
        {as: :check_boxes,
        checked: form_value.value.present?? form_value.value : []}
      else
        {as: :select,
          prompt: "Please select one option",
          selected: object.form_value.id}
      end
    end
  end
end
