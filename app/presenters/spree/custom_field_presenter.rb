module Spree
  class CustomFieldPresenter < SimpleDelegator
    def self.decorate_collection(custom_fields)
      custom_fields.map { |c| new(c) }
    end

    def object
      __getobj__
    end

    def display_label
      object.label.titleize
    end

    def input_html
      {class: 'fullwidth'}
    end

    def wrapper_html
      {}
    end

    def options
      {}
    end

    def merge_adjustments
      Proc.new{ |key, oldValue, newValue| key == :class ? "#{oldValue} #{newValue}" : newValue }
    end

    def required_html
      object.required?? 'true' : nil
    end
  end
end
