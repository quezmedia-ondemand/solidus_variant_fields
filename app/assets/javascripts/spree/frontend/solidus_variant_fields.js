// Placeholder manifest file.
// the installer will append this file to the app vendored assets here: vendor/assets/javascripts/spree/frontend/all.js'

Spree.ready(function($) {
  var radios, selectedRadio;
  Spree.updateResourceFieldsPresentation = function(resource_fields_element) {
    var hash_values;
    hash_values = resource_fields_element.data();
    if (Object.keys(hash_values.values).length > 0) {
      dl_elem = $("<dl/>", {
        class: 'product-form-values',
      });
      values_obj = hash_values.values;
      for (var property in values_obj) {
        if(values_obj.hasOwnProperty(property)){
          dt_elem = $("<dt/>", {
            text: property.replace(/\_/g," ")
          });
          span = $("<span/>",{text: values_obj[property].replace(/\_/g," ")});
          dd_elem = $("<dd/>", {
            html: span
          });
          dl_elem.append(dt_elem).append(dd_elem);
        }
      }
      return $("#form-values-container").html(dl_elem);
    }
  };
  radios = $('#product-variants input[type="radio"]');
  return radios.click(function(event) {
    variant_elem = $(this);
    split_variant_id_attr = variant_elem.attr('id').split('_')
    variant_id = split_variant_id_attr[split_variant_id_attr.length-1]
    resource_fields_element = variant_elem.next(".variant-form-values-" + variant_id);
    return Spree.updateResourceFieldsPresentation(resource_fields_element);
  });
});
