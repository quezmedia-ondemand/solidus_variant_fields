module Spree
  module PermissionSets
    class VariantFieldsDisplay < PermissionSets::Base
      def activate!
        can [:display, :admin, :edit], Spree::ResourceType
      end
    end
  end
end
