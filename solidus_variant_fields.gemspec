# encoding: UTF-8
$:.push File.expand_path('../lib', __FILE__)
require 'solidus_variant_fields/version'

Gem::Specification.new do |s|
  s.platform    = Gem::Platform::RUBY
  s.name        = 'solidus_variant_fields'
  s.version     = '1.0'
  s.summary     = 'Basic Functionality That Provides A Way For Adding Custom Fields For Variants'
  s.description = 'An Admin Should Be Able To Admin Custom Fields To Any Variant And Set This To Be Visible At Frontend Store'
  s.license     = 'BSD-3-Clause'
  s.required_ruby_version = ">= 2.1"

  s.author    = 'Hugo Hernani'
  s.email     = 'hugo@quezmedia.com'
  s.homepage  = 'https://bitbucket.org/quezmedia-ondemand/solidus_variant_fields/'

  s.files        = `git ls-files`.split("\n")
  s.test_files   = `git ls-files -- spec/*`.split("\n")
  s.require_path = "lib"
  s.requirements << "none"

  solidus_version = [">= 1.0.6", "< 3"]

  s.add_dependency 'solidus_core', solidus_version

  s.add_dependency 'deface', '~> 1.0'
  s.add_dependency 'simple_form'

  s.add_development_dependency "solidus_backend", solidus_version
  s.add_development_dependency "solidus_frontend", solidus_version
  s.add_development_dependency 'capybara'
  s.add_development_dependency "shoulda-matchers"
  s.add_development_dependency 'poltergeist'
  s.add_development_dependency "rspec-rails", "~> 3.3"
  s.add_development_dependency 'rubocop'
  s.add_development_dependency 'rubocop-rspec'
  s.add_development_dependency 'pry-rails', '~> 0.3.4'
end
