class AddAttrChToSpreeCustomField < ActiveRecord::Migration[5.0]
  def change
    add_column :spree_custom_fields, :as_check_boxes, :boolean, default: false
  end
end
