class CreateSpreeDropdownOptions < ActiveRecord::Migration[5.0]
  def change
    create_table :spree_dropdown_options do |t|
      t.string :label
      t.references :dropdown_field, index: true, foreign_key: { to_table: :spree_custom_fields }

      t.timestamps
    end
  end
end
