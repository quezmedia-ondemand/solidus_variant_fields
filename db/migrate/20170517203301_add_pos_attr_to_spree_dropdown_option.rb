class AddPosAttrToSpreeDropdownOption < ActiveRecord::Migration[5.0]
  def change
    add_column :spree_dropdown_options, :position, :integer
  end
end
