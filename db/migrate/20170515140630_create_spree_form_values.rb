class CreateSpreeFormValues < ActiveRecord::Migration[5.0]
  def change
    create_table :spree_form_values do |t|
      t.string :value
      t.references :custom_field, index: true, foreign_key: { to_table: :spree_custom_fields }
      t.references :holder, polymorphic: true, index: true

      t.timestamps
    end
  end
end
