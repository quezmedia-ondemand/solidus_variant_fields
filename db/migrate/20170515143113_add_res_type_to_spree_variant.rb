class AddResTypeToSpreeVariant < ActiveRecord::Migration[5.0]
  def change
    add_reference :spree_variants, :resource_type, index: true, foreign_key: { to_table: :spree_resource_types }
  end
end
