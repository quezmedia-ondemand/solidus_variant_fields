class AddPlaceHolderToSpreeCustomField < ActiveRecord::Migration[5.0]
  def change
    add_column :spree_custom_fields, :placeholder, :string
  end
end
