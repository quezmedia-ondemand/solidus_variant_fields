class ChangeRefResTypeAtSpreeVariant < ActiveRecord::Migration[5.0]
  def change
    remove_reference :spree_variants, :resource_type, index: true, foreign_key: { to_table: :spree_resource_types }
    add_reference :spree_variants, :resource_type, index: true, foreign_key: { to_table: :spree_resource_types }, on_delete: :nullify
  end
end
