class AddExtraAttrsToSpreeCustomField < ActiveRecord::Migration[5.0]
  def change
    add_column :spree_custom_fields, :max, :string
    add_column :spree_custom_fields, :min, :string
  end
end
