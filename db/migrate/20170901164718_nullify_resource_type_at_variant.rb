class NullifyResourceTypeAtVariant < ActiveRecord::Migration[5.0]
  def change
    remove_foreign_key :spree_variants, column: :resource_type_id
    add_foreign_key :spree_variants, :spree_resource_types, column: :resource_type_id, on_delete: :nullify
  end
end
