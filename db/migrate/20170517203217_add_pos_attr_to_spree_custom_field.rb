class AddPosAttrToSpreeCustomField < ActiveRecord::Migration[5.0]
  def change
    add_column :spree_custom_fields, :position, :integer
  end
end
