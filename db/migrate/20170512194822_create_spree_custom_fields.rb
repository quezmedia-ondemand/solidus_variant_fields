class CreateSpreeCustomFields < ActiveRecord::Migration[5.0]
  def change
    create_table :spree_custom_fields do |t|
      t.string :label
      t.boolean :required, default: false
      t.string :type
      t.boolean :frontend_visible, default: false
      t.references :owner, polymorphic: true, index: true

      t.timestamps
    end
  end
end
