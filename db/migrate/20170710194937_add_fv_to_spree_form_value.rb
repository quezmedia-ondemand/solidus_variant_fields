class AddFvToSpreeFormValue < ActiveRecord::Migration[5.0]
  def change
    add_column :spree_form_values, :frontend_visible, :boolean, default: false
  end
end
