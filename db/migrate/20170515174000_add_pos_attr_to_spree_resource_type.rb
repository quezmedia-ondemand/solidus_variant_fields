class AddPosAttrToSpreeResourceType < ActiveRecord::Migration[5.0]
  def change
    add_column :spree_resource_types, :position, :integer, default: 0
  end
end
