class AddFieldLToSpreeFormValue < ActiveRecord::Migration[5.0]
  def change
    add_column :spree_form_values, :field_label, :string
  end
end
