class ChangeForKeySpreeFormValue < ActiveRecord::Migration[5.0]
  def change
    remove_foreign_key :spree_form_values, column: :custom_field_id
    add_foreign_key :spree_form_values, :spree_custom_fields, column: :custom_field_id, on_delete: :nullify
  end
end
