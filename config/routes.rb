Spree::Core::Engine.routes.draw do
  namespace :admin do
    resources :products do
      resources :tab_resource_types, only: [:index, :update] do
        delete 'form_value/:form_value_id', to: 'tab_resource_types#delete_value', on: :collection, as: :form_value
      end
    end
    resources :resource_types do
      collection do
        post :update_positions
        post :update_values_positions
      end
      resources :custom_fields, controller: 'resource_types/custom_fields' do
        collection do
          post :update_positions
          post :update_values_positions
        end
      end
    end
  end
end
